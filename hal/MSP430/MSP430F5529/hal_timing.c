#ifndef _TIMING_H_
#error "Don't include this file in your project directly, it will be included by timing.c"
#endif
#include <stdint.h>
#include <msp430.h>
#include "timing.h"
#include "system.h"

#ifdef TIMING_USE_TIMER_B
	void hal_Timing_Init(void){
		TB0CCR0 = FCPU/1000;	//Set the period to 1ms
		TB0CTL &= ~TBIFG;	//Clear the interrupt flag
		TB0CTL |= TBSSEL_2 | MC_1 | TBCLR; //Set SMCLK, UP Mode
		TB0CCTL0 |= CCIE;
	}

	#ifdef MSPGCC
	__attribute__((interrupt(TIMER0_B0_VECTOR)))
	void TIMER0_B0_ISR(void) {
	#else
	#pragma vector=TIMER0_B0_VECTOR
	__interrupt void TIMER0_B0_ISR(void){
	#endif
		TimingISR();
		TB0CTL &= ~TBIFG;	//Clear the interrupt flag
	}
#else
	
	void hal_Timing_Init(void){
		TA0CCR0 = FCPU/1000;	//Set the period to 1ms
		TA0CTL &= ~TAIFG;	//Clear the interrupt flag
		TA0CTL |= TASSEL_2 | MC_1 | TACLR; //Set SMCLK, UP Mode
		TA0CCTL0 |= CCIE;
	}

	#ifdef MSPGCC
	__attribute__((interrupt(TIMER0_A0_VECTOR)))
	void TIMER0_A0_ISR(void) {
	#else
	#pragma vector=TIMER0_A0_VECTOR
	__interrupt void TIMER0_A0_ISR(void){
	#endif
		TimingISR();
		TA0CTL &= ~TAIFG;	//Clear the interrupt flag
	}
#endif

#define TICKS_PER_US (FCPU / 1000000)
#define TimingUsNow() TA0R / TICKS_PER_US

