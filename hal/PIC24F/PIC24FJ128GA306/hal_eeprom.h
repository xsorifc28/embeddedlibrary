/* 
 * File:   hal_eeprom.h
 * Author: Michael15
 *
 * Created on November 14, 2015, 4:25 PM
 */

#ifndef _HAL_EEPROM_H_
#define	_HAL_EEPROM_H_

#include "system.h"

void hal_EEPROM_Init(void);

uint16_t hal_EEPROM_Read(uint16_t address);

void hal_EEPROM_Write(uint16_t address, uint16_t data);

#endif	/* HAL_EEPROM_H */

