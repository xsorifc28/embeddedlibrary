/* 
 * File:   main.c
 * Author: George
 *
 * Created on March 8, 2015, 9:15 PM
 */

#include "system.h"

#define RF_SPI_CH 1

#ifndef SUBSYS_UART
#define SUBSYS_UART 2
#endif

void RF1_CE(uint8_t out);
void RF1_CSN(uint8_t out);
void RF1_PollIRQ(void);

void RF2_CE(uint8_t out);
void RF2_CSN(uint8_t out);
void RF2_PollIRQ(void);

void RF1_Init(void);
void RF2_Init(void);

void RF1_RxPayloadHandler(uint8_t * data, uint8_t length);
void RF1_AckPayloadHandler(uint8_t * data, uint8_t length);
void RF1_AckReceivedHandler(void);
void RF1_MaxRetriesHandler(void);

void RF2_RxPayloadHandler(uint8_t * data, uint8_t length);
void RF2_AckPayloadHandler(uint8_t * data, uint8_t length);
void RF2_AckReceivedHandler(void);
void RF2_MaxRetriesHandler(void);

void SendTest(void);
void ThisNode_TestMsgHandler(uint8_t * data, uint8_t length, uint8_t from);
void ThisNode_BombermanMsgHandler(uint8_t * data, uint8_t length, uint8_t from);
void ThisNode2_TestMsgHandler(uint8_t * data, uint8_t length, uint8_t from);

/* Variables */
nrfnet_t RF2_Net;

char test_msg[16] = {"Test Message!\r\n"};
char reply_msg[25] = {"Test Message Response!\r\n"};

int main(int argc, char** argv) {
    DisableInterrupts();
    Timing_Init();
    Task_Init();
    UART_Init(2);
    
    // Setup the SPI channel to be used by the NRF nodes
    spi_settings_t spi_settings;
    spi_settings.channel = RF_SPI_CH;
    spi_settings.bit_rate = 100000;
    spi_settings.hal_settings.mode16 = 0;
    spi_settings.hal_settings.mode32 = 0;
    spi_settings.hal_settings.sample_phase = 0;
    spi_settings.mode = 0;
    SPI_Init(&spi_settings);

    //Setup IRQ, CE, CSN
    TRISBbits.TRISB3 = 0; // CS1 output
    TRISBbits.TRISB2 = 0; // CE1 output
    TRISAbits.TRISA2 = 1; // IRQ1 input
    RF1_CSN(1);

    //Setup IRQ, CE, CSN
    TRISBbits.TRISB15 = 0; // CS2 output
    TRISAbits.TRISA0 = 0; // CE2 output
    TRISAbits.TRISA3 = 1; // IRQ2 input
    RF2_CSN(1);

    EnableInterrupts();

    // configure interrupt pin for RF1 module (A2)
    INT2R = 0; // map interrupt 2 to A0
    INTCONbits.INT2EP = 0; // set for falling edge trigger
    IFS0bits.INT2IF = 0; // clear flag
    IPC2bits.INT2IP = 1; // priority level 1
    IEC0bits.INT2IE = 1; // enable interrupt
    // configure interrupt pin for RF2 module (A3)
    INT1R = 0; // map interrupt 2 to A3
    INTCONbits.INT1EP = 0; // set for falling edge trigger
    IFS0bits.INT1IF = 0; // clear flag
    IPC1bits.INT1IP = 1; // priority level 1
    IEC0bits.INT1IE = 1; // enable interrupt

    nrf24_NetworkInit(RF1_CE, RF1_CSN, RF_SPI_CH);

    Log_EchoOn();

    bomberManInit();

    while(1){
        SystemTick();
    }
    
    return (EXIT_SUCCESS);
}


// This function is provided to the network layer in the init function and is used to control the
// Chip Enable pin on the radio
void RF1_CE(uint8_t out){
	LATBbits.LATB2 = out ? 1 : 0;
}

// This function is provided to the network layer in the init function and is used to control the
// Chip Select pin on the radio
void RF1_CSN(uint8_t out){
	LATBbits.LATB3 = out ? 1 : 0;
}

void __attribute__((vector(_EXTERNAL_2_VECTOR), interrupt(), nomips16)) _RF1_ISR(void) {
    nrf24_NetworkISRHandler();
    IFS0bits.INT2IF = 0;
}

// This function is provided to the network layer in the init function and is used to control the
// Chip Enable pin on the radio
void RF2_CE(uint8_t out){
	LATAbits.LATA0 = out ? 1 : 0;
}

// This function is provided to the network layer in the init function and is used to control the
// Chip Select pin on the radio
void RF2_CSN(uint8_t out){
  LATBbits.LATB15 = out ? 1 : 0;
}

void __attribute__((vector(_EXTERNAL_1_VECTOR), interrupt(), nomips16)) _RF2_ISR(void) {
    nrf24_NetworkISRHandlerN(RF2_Net);
    IFS0bits.INT1IF = 0;
}

