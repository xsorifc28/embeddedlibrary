#ifndef _TEA_H_
#define _TEA_H_

#include "big_int.h"

/** @brief TEA (Tiny Encryption Algorithm) module
 *
 * @defgroup tea Tiny Encryption Algorithm
 * @addtogroup JJ_chat
 *
 * Takes a 32-bit number and 128-bit key and uses it to encrypt a message or decrypt it. 
 * The same key must be used for both encryption and decryption. This is quite a fast encryption.
 * Note that both users must share the key outside of the communication
 * channel (e.g. on paper).
 *
 * To learn about the 
 * TEA please see <a href="http://en.wikipedia.org/wiki/Tiny_Encryption_Algorithm">this wikipedia page</a> but in short. the TEA
 * splits the key into 8 bits and the message into 16. It takes one 16 bit message and does the following
 * sum= ((message0<<4 + key0 | message0 + delta[0] | message0>>5 + key1)+message1). This is then done again  
 * where message0 is replaced with sum , key0 and key1 become the second 16 bits of the key, message1 becomes 
 * message0, and delta increments. This is done 32 times to produce security through
 * obscurity.
 *
 * @author Jonathan Whalen
 */

/**
 * @brief Encrypts a message
 * 
 * Give the method 2 pointers to data the message and key and it will encrypt the message.
 * 
 * @param msg pointer to the 64-bit char array to be encrypted 
 * @param key the big_int_t 128-bit key to be used
 *
 */

#define  DELTA 0x9E3779B9
#define  UNDELTA 0xC6EF3720


void encrypt (char *msg, big_int_t *key);

/** @brief Decrypts a message using the given key
 * 
 * Give the method 2 pointers to data the message and key and it will decrypt the message.
 * 
 * @param msg pointer to the 64-bit char array to be encrypted 
 * @param key the big_int_t 128-bit key to be used 
 */
void decrypt (char *msg, big_int_t *key);

/**
 * @brief Generates a key
 * 
 * When used it will return a randomly generated 128-bit key for encryption.
 * 
 * @param key pointer to a big_int_t to store the key
 */
void genkey(big_int_t *key);

#endif /* _TEA_H_ */
