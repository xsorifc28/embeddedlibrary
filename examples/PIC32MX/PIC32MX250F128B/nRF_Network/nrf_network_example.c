#include "system.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/**
 * NRF24 Network Example
 *
 * This example demonstrates very basic use of the NRF Network on the PIC32MX.
 * The example uses two radios both connected to a single PIC32MX.

 * In this example, the network is configured and some simple messages are sent between the two nodes.
 * The first node is configured using the default network built into the network layer.  The second node
 * requires that the user interface with the extended functionality of the network layer.  For most use cases,
 * there will only be one node on a single uC and therefore, the extended functionality is not needed.  See documentation
 * for the nrf network layer and inline comments below for more information about the extended functionality.
 *
 * The first node, using the default network built into the network layer, is configured using the THIS_NODE macro found in
 * system.h  You must always define this macro in the system.h file to use the network layer. In this example, we want the
 * first node to be a branch (Meaning it has a parent (the THIS_NODE2) and children). Most users on a network will be configured
 * as either a leaf or a branch, since only one user can be configured as a THIS_NODE2.
 *
 * The second node, using the extended functionality, is setup to be the THIS_NODE2 of the network.
 *
 * The first node (the branch) sets up a message handler to listen for TEST_MSG's, while the second node (the THIS_NODE2) sets
 * up a message handler to listen for TEST_MSG's.  (TEST_MSG and TEST_MSG were arbitrarily chosen and do not implement any of the
 * functionality of those application layers)
 *
 * A periodic task is setup to send a TEST_MSG to THIS_NODE from the THIS_NODE2.  The THIS_NODE node has a listener configured to handle
 * reception of TEST_MSG.  In the handler, the received message is simply printed over UART2.  In addition, the THIS_NODE node, in turn,
 * writes a TEST_MSG back to the THIS_NODE2.  Remember this is for demonstration purposes only.  Typically the THIS_NODE2 and branch nodes would
 * both have message handlers for the same type of system and the THIS_NODE node would just respond with the same message type.
 *
 * When the THIS_NODE2 receives the TEST_MSG, the handler is called and the received message is simply printed to the terminal over UART2.
 *
 * Be sure to read the inline comments below to get a better understanding of how to implement applications using the network layer.
 *
 * To test this example, simply run the program with a terminal setup on UART2 and look for
 *
 * Test Message!
 * Test Message Response!
 *
 */

/*	Macros */
#define RF_SPI_CH 1

#ifndef SUBSYS_UART
#define SUBSYS_UART 2
#endif

/* Function Declarations */
void RF1_CE(uint8_t out);
void RF1_CSN(uint8_t out);
void RF1_PollIRQ(void);

void RF2_CE(uint8_t out);
void RF2_CSN(uint8_t out);
void RF2_PollIRQ(void);

void RF1_Init(void);
void RF2_Init(void);

void RF1_RxPayloadHandler(uint8_t * data, uint8_t length);
void RF1_AckPayloadHandler(uint8_t * data, uint8_t length);
void RF1_AckReceivedHandler(void);
void RF1_MaxRetriesHandler(void);

void RF2_RxPayloadHandler(uint8_t * data, uint8_t length);
void RF2_AckPayloadHandler(uint8_t * data, uint8_t length);
void RF2_AckReceivedHandler(void);
void RF2_MaxRetriesHandler(void);

void SendTest(void);
void ThisNode_TestMsgHandler(uint8_t * data, uint8_t length, uint8_t from);
void ThisNode2_TestMsgHandler(uint8_t * data, uint8_t length, uint8_t from);

/* Variables */
nrfnet_t RF2_Net;

char test_msg[16] = {"Test Message!\r\n"};
char reply_msg[25] = {"Test Message Response!\r\n"};

int main(void) {
    DisableInterrupts();

    Timing_Init();
    Task_Init();
    UART_Init(2);

    // Setup the SPI channel to be used by the NRF nodes
    spi_settings_t spi_settings;
    spi_settings.channel = RF_SPI_CH;
    spi_settings.bit_rate = 100000;
    spi_settings.hal_settings.mode16 = 0;
    spi_settings.hal_settings.mode32 = 0;
    spi_settings.hal_settings.sample_phase = 0;
    spi_settings.mode = 0;
    SPI_Init(&spi_settings);

    //Setup IRQ, CE, CSN
    TRISBbits.TRISB3 = 0; // CS1 output
    TRISBbits.TRISB2 = 0; // CE1 output
    TRISAbits.TRISA2 = 1; // IRQ1 input
    RF1_CSN(1);

    //Setup IRQ, CE, CSN
    TRISBbits.TRISB15 = 0; // CS2 output
    TRISAbits.TRISA0 = 0; // CE2 output
    TRISAbits.TRISA3 = 1; // IRQ2 input
    RF2_CSN(1);

    EnableInterrupts();
    
    // configure interrupt pin for RF1 module (A2)
    INT2R = 0; // map interrupt 2 to A0
    INTCONbits.INT2EP = 0; // set for falling edge trigger
    IFS0bits.INT2IF = 0; // clear flag
    IPC2bits.INT2IP = 1; // priority level 1
    IEC0bits.INT2IE = 1; // enable interrupt
    // configure interrupt pin for RF2 module (A3)
    INT1R = 0; // map interrupt 2 to A3
    INTCONbits.INT1EP = 0; // set for falling edge trigger
    IFS0bits.INT1IF = 0; // clear flag
    IPC1bits.INT1IP = 1; // priority level 1
    IEC0bits.INT1IE = 1; // enable interrupt

    // Setup the default network, this uses the THIS_NODE macro found in system.h
    nrf24_NetworkInit(RF1_CE, RF1_CSN, RF_SPI_CH);
    // Setup the second node as THIS_NODE2 using the extended network framework used on systems with
    // multiple nodes
    nrf24_NetworkInitN(&RF2_Net, RF2_CE, RF2_CSN, RF_SPI_CH, THIS_NODE2);

    // Register a message handler on the THIS_NODE node listening for TEST_MSGs
    nrf24_RegisterMsgHandler(TEST_MSG, ThisNode_TestMsgHandler);
    // Register a message handler on the THIS_NODE2 node listening for TEST_MSGs
    nrf24_RegisterMsgHandlerN(&RF2_Net, TEST_MSG, ThisNode2_TestMsgHandler);

    // Schedule a task to send a TEST_MSG from the THIS_NODE2 node to THIS_NODE every 1 second
    Task_Schedule(SendTest, 0, 500, 1000);
    Log_EchoOn();
    
	while(1){
		SystemTick();
	}
}

void SendTest(void){
	// Send a TEST_MSG to THIS_NODE from THIS_NODE2
	nrf24_SendMsgN(&RF2_Net, THIS_NODE, TEST_MSG, &test_msg[0], 15);
}

void ThisNode_TestMsgHandler(uint8_t * data, uint8_t length, uint8_t from){
	// When we receive the message from the THIS_NODE2, respond with a TEST_MSG
	nrf24_SendMsg(THIS_NODE2, TEST_MSG, &reply_msg[0], 24);
	// Print the received message to the terminal
	UART_Printf(2, "Merlino Received: ");
	UART_Write(2, data, length);
}

void ThisNode2_TestMsgHandler(uint8_t * data, uint8_t length, uint8_t from){
	// Print the received message to the terminal
	UART_Printf(2, "Master Received: ");
	UART_Write(2, data, length);
}

// This function is provided to the network layer in the init function and is used to control the
// Chip Enable pin on the radio
void RF1_CE(uint8_t out){
	LATBbits.LATB2 = out ? 1 : 0;
}

// This function is provided to the network layer in the init function and is used to control the
// Chip Select pin on the radio
void RF1_CSN(uint8_t out){
	LATBbits.LATB3 = out ? 1 : 0;
}

void __attribute__((vector(_EXTERNAL_2_VECTOR), interrupt(), nomips16)) _RF1_ISR(void) {
    nrf24_NetworkISRHandler();
    IFS0bits.INT2IF = 0;
}

// This function is provided to the network layer in the init function and is used to control the
// Chip Enable pin on the radio
void RF2_CE(uint8_t out){
	LATAbits.LATA0 = out ? 1 : 0;
}

// This function is provided to the network layer in the init function and is used to control the
// Chip Select pin on the radio
void RF2_CSN(uint8_t out){
  LATBbits.LATB15 = out ? 1 : 0;
}

void __attribute__((vector(_EXTERNAL_1_VECTOR), interrupt(), nomips16)) _RF2_ISR(void) {
    nrf24_NetworkISRHandlerN(RF2_Net);
    IFS0bits.INT1IF = 0;
}
