/** 
 * @defgroup hal_spi SPI Hardware Abstraction Layer
 * @ingroup spi
 *
 * @author Anthony Merlino
 * @author Michael Muhlbaier
 *
 * Created on February 26, 2015, 1:31 PM
 *
 * The Hardware Abstraction Layer for SPI is very processor specific and a
 * generic hal_spi.h would not work for each processor. Thus hal_spi.h must
 * be created for each processor which is to support the SPI module. A typical
 * hal_spi.h would include definitions for the channel names and a settings
 * structure for configuring the SPI module(s).
 */
