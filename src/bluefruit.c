#include "bluefruit.h"

void bluefruit_Init(void){
	//UART_RegisterReceiver(BLUEFRUIT_CHANNEL, bluefruit_charReceiver);
}

/*
char bluefruit_rx_buffer[BLUEFRUIT_RECEIVE_LENGTH];
void bluefruit_charReceiver(char uart_char){
	static enum com_state bluefruit_receive_state = STATE_IDLE;
	static int length;
	switch(com_state){
	case STATE_IDLE:
		if(uart_char=='O') com_state = STATE_OK_1;
		else if(uart_char=='E') com_state = STATE_ERROR_1;
		break;
	case STATE_OK_1:
		if(uart_char=='K')
		return;
	case STATE_OK_2:
		return;
	case STATE_ERROR_1:
		return;
	case STATE_ERROR_2:
		return;
	case STATE_ERROR_3:
		return;
	case STATE_ERROR_4:
		return;
	case STATE_ERROR_5:
		return;
	default:
		return;
	}
}
*/

void bluefruit_processBuffer(void){
	//TODO
}

void bluefruit_SwitchMode(void){
	UART_Printf(BLUEFRUIT_CHANNEL, "+++\r\n");
}

void bluefruit_AT(void){
	UART_Printf(BLUEFRUIT_CHANNEL, "AT\r\n");
}
void bluefruit_ATZ(void){
	UART_Printf(BLUEFRUIT_CHANNEL, "ATZ\r\n");
}

void bluefruit_FactoryReset(void){
	UART_Printf(BLUEFRUIT_CHANNEL, "AT+FACTORYRESET\r\n");
}
void bluefruit_DFU(void){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+DFU\r\n");
}

void bluefruit_GATTClear(void){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTCLEAR\r\n");
}

void bluefruit_GATTAddService(char * uuid){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTADDSERVICE=UUID=%s\r\n", uuid);
}

void bluefruit_GATTAddService128(char * uuid){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTADDSERVICE=UUID128=0x%s\r\n", uuid);
}

void bluefruit_GATTAddChar(char * uuid, char * properties, uint16_t min_length, uint16_t max_length, char * value){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTADDCHAR=UUID=%s,PROPERTIES=%s,MIN_LEN=%d,MAX_LEN=%d,VALUE=%s\r\n",
			uuid, properties, min_length, max_length, value);
}

void bluefruit_GATTAddChar128(char * uuid, char * properties, uint16_t min_length, uint16_t max_length, char * value){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTADDCHAR=UUID128=%s,PROPERTIES=%s,MIN_LEN=%d,MAX_LEN=%d,VALUE=%s\r\n",
				uuid, properties, min_length, max_length, value);
}

void bluefruit_GATTReadChar(uint8_t id){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTCHAR=%d\r\n", id);
}

void bluefruit_GATTWriteChar(uint8_t id, char * data){
	UART_Printf(BLUEFRUIT_CHANNEL,"AT+GATTCHAR=%d,%s\r\n", id, data);
}
