#include "system.h"
#ifndef _WATCHDOG_H_
#error "Please include watchdog.h in system.h"
#endif

void WatchdogTask(void);

void Watchdog_Init(void) {
    static uint8_t initialized = 0;
    if(initialized == 0) {
        Task_Schedule(WatchdogTask, 0, 1555, 1000);
        initialized = 1;
    }
}

uint8_t reset_watchdog = 0;

void SetResetWatchdogFlag(void) {
    reset_watchdog = 1;
}

void WatchdogTask(void) {
    if(reset_watchdog) {
        hal_ClearWatchdog();
        reset_watchdog = 0;
    }
}