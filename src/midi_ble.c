/** 
 * File:   midi_ble.c
 * Author: r.o.c.k.e.t.
 * Author: Andrew D.
 * Author: Mike Mc.
 * Created on March 28, 2016, 1:29 PM
 */
#include "midi_ble.h"
#include "bluefruit.h"

void MIDI_BLE_Init(void){
    bluefruit_Init();
    char * uuid = "03B80E5AEDE84B33A7516CE34EC4C700";
    bluefruit_GATTAddService128(uuid);
    char * tx_uuid = "7772E5DB38684112A1A9F2669D106BF3";
    bluefruit_GATTAddChar128(tx_uuid, GATTPROP_WRITE, 5, 64, "00000");
    MIDI_Init(MIDI_BLE_Send);
}

void MIDI_BLE_Send(uint8_t data[], uint8_t length){
    uint16_t timestamp = TimeNow();
    
    uint8_t header_byte = (timestamp >> 7) & 0b00111111; // get bits 12-7 of timestamp and truncate to 8 bits
    header_byte |= 0b10000000; // append start bit and reserved bit front of timestamp via mask, result: [header_byte] = [start_bit][reserved_bit][timestamp(12:7)]
    
    uint8_t timestamp_byte = timestamp & 0b01111111; // truncates to lower byte
    timestamp_byte |= 0b10000000; // add start byte to front via mask, result: [timestamp_byte] = [start_bit][timestamp(6:0)]
    
    uint8_t MIDI_status = data[0];
    uint8_t MIDI_event1 = data[1];
    uint8_t MIDI_event2 = data[2];
    
    uint8_t packet[64];
    
    packet[0] = header_byte;
    packet[1] = timestamp_byte;
    memcpy(&packet[2], data, length);
    packet[length+3] = 0;
    
    bluefruit_GATTWriteChar(1, packet);//length in bits?=72 in BYTES=9//ask
}

void MIDI_BLE_Receive(uint8_t data){
    
}//Ignore for now