/** 
 * File:   midi_ble.h
 * Author: Michael
 *
 * Created on March 22, 2016, 2:57 PM
 * 
 * @defgroup midi_ble MIDI BLE using Apple's Spec
 * @ingroup midi
 * @{
 */

#ifndef MIDI_BLE_H
#define	MIDI_BLE_H

#include "stdint.h"

#ifdef	__cplusplus
extern "C" {
#endif

/**
 * 
 */
void MIDI_BLE_Init(void);

/**
 * 
 * @param data
 * @param length
// needs to generate and update 13bit ms timestamp
// each BLE packet is:
// [header][timestamp 1][midi msg][timestamp 2][midi msg 2]...
// except for system exclusive message which is:
// [header][timestamp][midi msg]
// [header][rest of midi msg without EOX][timestamp][EOX]
 * */
void MIDI_BLE_Send(uint8_t data[], uint8_t length);

/**
 * 
 * @param data
 */
void MIDI_BLE_Receive(uint8_t data);

#ifdef	__cplusplus
}
#endif
/** @} */
#endif	/* MIDI_BLE_H */

