#ifndef _BLUEFRUIT_H_
#define	_BLUEFRUIT_H_

#include "system.h"
#include "uart.h"
#include <stdint.h>

#ifndef BLUEFRUIT_CHANNEL
#define BLUEFRUIT_CHANNEL 0
#endif

/*
static enum bluefruit_receive_state {
	STATE_IDLE, STATE_OK_1, STATE_OK_2, STATE_ERROR_1, STATE_ERROR_2, STATE_ERROR_3, STATE_ERROR_4, STATE_ERROR_5
};

typedef uint8_t BLUEFRUIT_RESP;
typedef char * BLUEFRUIT_DATA;

//Bluefruit Error Codes
#define RESP_OK 1
#define RESP_ERROR 2
*/

//GATT Properties
#define GATTPROP_READ "0x02"
#define GATTPROP_WRITE_WO_RESP "0x04"
#define GATTPROP_WRITE "0x08"
#define GATTPROP_NOTIFY "0x10"
#define GATTPROP_INDICATE "0x20"

/*
typedef struct {
	BLUEFRUIT_RESP resp;
	BLUEFRUIT_DATA data;
} BLUEFRUIT_RESP_BUNDLE;
*/

//typedef void(*bluefruitRespCallback_t)(BLUEFRUIT_RESP_BUNDLE bundle);

void bluefruit_Init(void);

void bluefruit_charReceiver(char);

void bluefruit_SwitchMode(void);

void bluefruit_AT(void);
void bluefruit_ATZ(void);

void bluefruit_FactoryReset(void);
void bluefruit_DFU(void);

void bluefruit_GATTClear(void);
void bluefruit_GATTAddService(char * uuid);
void bluefruit_GATTAddService128(char * uuid);
void bluefruit_GATTAddChar(char * uuid, char * properties, uint16_t min_length, uint16_t max_length, char * value);
void bluefruit_GATTAddChar128(char * uuid, char * properties, uint16_t min_length, uint16_t max_length, char * value);
void bluefruit_GATTReadChar(uint8_t id);
void bluefruit_GATTWriteChar(uint8_t id, char * data);

#endif	/* BLUEFRUIT_H */

