/** @defgroup spi SPI Module
 *
 * File:   spi.h
 * Author: Anthony Merlino
 *
 * @todo Anthony Merlino document this module's main block
 * @todo MM divy up documentation of functions after they are covered in class
 *
 * Created on February 24, 2015, 11:03 PM
 * @{
 */

#ifndef _SPI_H_
#define	_SPI_H_

#ifndef SPI_MAX_SIZE
#define SPI_MAX_SIZE 8 ///< max length
#endif

#ifndef SPI_MAX_TRANSACTIONS
#define SPI_MAX_TRANSACTIONS 4 ///< max queued transactions
#endif

#include "hal_spi.h"

/// spi settings structure
typedef struct spi_settings_t {
    uint8_t channel; ///< channel number of module to be configured
    uint32_t bit_rate; ///< bit_rate of SPI clock, not guaranteed to be accurate,
    /// depends on processor clock settings and SPI peripheral
    uint8_t mode; ///< SPI mode select
    /// - Mode 0 idle clock is low, data changes on high to low clock edge
    /// - Mode 1 idle clock is low, data changes on low to high clock edge
    /// - Mode 2 idle clock is high, data changes on low to high clock edge
    /// - Mode 3 idle clock is high, data changes on high to low clock edge
    hal_spi_settings_t hal_settings; ///< processor specific settings
} spi_settings_t;

/// spi transaction structure
struct spi_transaction {
    uint8_t writeLength; ///< write length
    uint8_t readLength; ///< read length
    uint8_t readDelay; ///< read delay
    struct {
        volatile uint8_t blocking : 1; ///< blocking flag
        volatile uint8_t finished : 1; ///< transaction finished flag
        volatile uint8_t channel : 3; ///< channel setting
        volatile uint8_t unused : 3; ///< unused
    } flags;
    void (*callback)(struct spi_transaction *); ///< callback function
    void (*cs_control)(uint8_t); ///< cs_control
    uint8_t data[SPI_MAX_SIZE]; ///< data (write data followed by read data)
};

/// spi transaction typedef
typedef struct spi_transaction spi_transaction_t;

/**
 *
 * @param spi_settings
 */
void SPI_Init(spi_settings_t* spi_settings);

/**
 *
 * @param transaction
 *
 * @warning Not interrupt safe
 */
error_t SPI_Transact(spi_transaction_t * transaction);

/**
 *
 * @param channel
 */
void SPI_ISR(uint8_t channel);

/******************************************
 * HAL Functions
 *****************************************/
/**
 *
 * @param settings
 */
void hal_SPI_Init(spi_settings_t* settings);

///**
// *
// * @param channel
// */
//void hal_SPI_Enable(uint8_t channel);
//
///**
// *
// * @param channel
// */
//void hal_SPI_Disable(uint8_t channel);
//
///**
// *
// * @param channel
// * @return
// */
//uint8_t hal_SPI_SpaceAvailable(uint8_t channel);
//
///**
// *
// * @param channel
// * @return
// */
//uint8_t hal_SPI_DataAvailable(uint8_t channel);
//
///**
// *
// * @param channel
// */
//void hal_SPI_ClearRxIF(uint8_t channel);
//
///**
// *
// * @param channel
// */
//void hal_SPI_ClearTxIF(uint8_t channel);
//
///**
// *
// * @param channel
// */
//void hal_SPI_EnableRxInterrupt(uint8_t channel);
//
///**
// *
// * @param channel
// */
//void hal_SPI_EnableTxInterrupt(uint8_t channel);
//
///**
// *
// * @param channel
// */
//void hal_SPI_DisableRxInterrupt(uint8_t channel);
//
///**
// *
// * @param channel
// */
//void hal_SPI_DisableTxInterrupt(uint8_t channel);
//
///**
// *
// * @param channel
// */
//uint8_t hal_SPI_TxIntStatus(uint8_t channel);
//
///**
// *
// * @param channel
// */
//uint8_t hal_SPI_RxIntStatus(uint8_t channel);
//
///**
// *
// * @param channel
// */
//uint8_t hal_SPI_IsTxIntEnabled(uint8_t channel);
//
///**
// *
// * @param channel
// * @return
// */
//uint8_t hal_SPI_RxByte(uint8_t channel);
//
///**
// *
// * @param channel
// * @param b
// */
//void hal_SPI_TxByte(uint8_t channel, uint8_t b);

///@}
#endif // _SPI_H_
