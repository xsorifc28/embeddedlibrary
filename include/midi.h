/**
 *
 * File:   midi.h
 *
 * @author Nick Felker
 * @author Andrew DeMartino
 * @author ~(Kevin)
 * @author Michael McCaffrey
 * @author R.O.C.K.E.T.
 * @author Michael Muhlbaier
 *
 *
 * Created on February 21, 2016, 5:15 PM
 *
 * @defgroup midi MIDI Module
 * @{
 */
#ifndef _MIDI_H_
#define _MIDI_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define CHANNEL_1 0
#define CHANNEL_2 1
#define CHANNEL_3 2
#define CHANNEL_4 3
#define CHANNEL_5 4
#define CHANNEL_6 5
#define CHANNEL_7 6
#define CHANNEL_8 7
#define CHANNEL_9 8
#define CHANNEL_10 9
#define CHANNEL_11 10
#define CHANNEL_12 11
#define CHANNEL_13 12
#define CHANNEL_14 13
#define CHANNEL_15 14
#define CHANNEL_16 15
#define CHANNEL_DEFAULT CHANNEL_1

#define MAX_VELOCITY 127
#define PAN_LEFT 0
#define PAN_RIGHT 127
#define PAN_MIDDLE 63

/** MIDI transmitter prototype typedef
*
* Sets up the MIDI transmit type
* @param data -- pointer to the memory address data being send
* @param length -- length of the data being sent
*/
typedef void (*MIDI_tx_t) (uint8_t * data, uint8_t length);
/**
 * Initializes MIDI module
 * @param transmitter -- MIDI type containing a data pointer and length
 */
void MIDI_Init(MIDI_tx_t transmitter);
/**
 * Receives data compliant to MIDI standard from computer
 * @param data -- data recieved from the computer
 */
void MIDI_Receive(uint8_t data);
/**
 * Sends note data compliant to MIDI standard to computer
 * @param channel -- unsigned int referring to instrument sending note compliant to MIDI standard
 * @param note -- note to be played by the computer compliant to MIDI standard
 * @param velocity -- velocity of the note being played compliant to MIDI standard
 */
void MIDI_SendNote(uint8_t channnel, uint8_t note, uint8_t velocity);
/**
 * Sends a real-time system message to the device connected to the MIDI device
 * @param msg -- The status code that you want sent
 */
void MIDI_SendSysRTMsg(uint8_t msg);
/**
 * Sends a message to a specific channel of the MIDI program
 * @param channel -- The specific channel you want to message
 * @param msg -- The status code that you want sent
 * @param value -- Some statuses include a value. You can pass that here
 */
void MIDI_SendChannelModeMsg(uint8_t channel, uint8_t msg, uint8_t value);
/**
 * Stops a specific note from playing
 * @param channel -- The channel you want to access
 * @param note -- The note that you want turned off
 */
void MIDI_SendNoteOff(uint8_t channel, uint8_t note);
/**
 * Applies an aftertouch for a specific channel
 * @param channel -- The channel you want to access
 * @param velocity -- The amount of velocity (0-127)
 */
void MIDI_ChannelAftertouch(uint8_t channel, uint8_t velocity);
/**
 * Bends the pitch instead of a monophonic sound
 * @param channel -- The channel you want to access
 * @param value -- A 14-bit number providing the amount of pitchbend you want applied
 */
void MIDI_PitchBend(uint8_t channel, uint16_t value);
/**
 * Applies an aftertouch to a specific note on a channel
 * @param channel -- The channel you want to access
 * @param note -- The note you want to apply an aftertouch to
 * @param velocity -- The amount of aftertouch (0-127)
 */
void MIDI_PolyphonicAftertouch(uint8_t channel, uint8_t note, uint8_t velocity);
/**
 * Some MIDI programs allow you to change the instrument that is being used for playback
 * @param channel -- The channel you want to access
 * @param newProgram -- The new program/instrument you want to play (0-127)
 */
void MIDI_ProgramChange(uint8_t channel, uint8_t newProgram);

//Channel Messages

/**
 * Forces a reset of your instrument; turns off all sounds. This
 * should be used sparingly -- only if notes are not being turned
 * off properly
 * @param channel -- The channel you want to reset
 */
void MIDI_Panic(uint8_t channel);
/**
 * Modulates the tone of the instrument as it is being played
 * @param channel -- The channel you want to access
 * @param modulation_value -- The amount of modulation applied (0-127)
 */
void MIDI_Modulation(uint8_t channel, uint8_t modulation_value);
/**
 * Turns on the damper pedal. When notes are turned off, they'll fade out slowly
 * @param channel -- The channel you want to access
 */
void MIDI_DamperPedalOn(uint8_t channel);
/**
 * Turns off the damper pedal. When notes are turned off, they turn off suddenly
 * @param channel -- The channel you want to access
 */
void MIDI_DamperPedalOff(uint8_t channel);
/**
 * Adjusts the volume of the channel
 * @param channel -- The channel you want to access
 * @param newVolume -- The new volume for this channel (0-127)
 */
void MIDI_SetChannelVolume(uint8_t channel, uint8_t newVolume);
/**
 * Adjusts the relative position of the sound between two speakers
 * This can be used to generate stereo music
 * @param channel -- The channel you want to access
 * @param direction -- The direction of the channel. 0 is fully left. 127 is fully right. 63 is default.
 */
void MIDI_SetPan(uint8_t channel, uint8_t direction);
/**
 * Enables a portamento control. This allows your instrument to glide
 * from the current note to a new note without a new attack.
 *
 * To enable this, you first must set the PTC for an initial note
 * on a channel. Next, call a Note-On message for your next note.
 * There will be a glide between the two notes.
 *
 * The portamento control only affects this Note-On message. It is
 * reset afterwards.
 *
 * @param channel -- The channel you want to access
 * @param fromNote -- The note you want to begin with
 */
void MIDI_PortamentoControl(uint8_t channel, uint8_t fromNote);

//System Real-Time Messages

/**
 * Sends a message to the master controller to begin the song
 * or sequence
 */
void MIDI_SequenceStart();
/**
 * Sends a message to the master controller to stop playback
 */
void MIDI_SequenceStop();
/**
 * Sends a message to the master controller to start playing at
 * the current location
 */
void MIDI_SequenceContinue();
/**
 * Sends non-note data compliant to MIDI standard to computer
 * @param data -- data to be sent to computer
 * @param length -- length of data being sent
 */
void MIDI_SendMsg(uint8_t * data, uint8_t length);
/**
 * @}
 */
#ifdef  __cplusplus
}
#endif
#endif /* MIDI_H */