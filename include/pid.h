/**
 * @file pid.h
 *
 * @defgroup control Control Subsystem
 *
 *  Created on: Mar 17, 2014
 *      @author: Bradley Ebinger
 *  Updated on: Feb 15, 2015
 *      @author: Anthony Merlino
 *
 * @version 2015.02.15
 * @{
 */


#ifndef PID_H
#define PID_H

#include <stdint.h>
#include <stdbool.h>

/**
 * Structure to hold PID controller data
 *
 *  - kp, ki, and kd are the gain coefficients of the proportional, integral
 * and derivative terms respectively
 *
 *  - prev_error - the error computed last time step to be used for derivative computation
 *
 *  - accum_error - the total accumulated error of the system to be used for integral computation
 *
 *  - prev_time - the last time the PID was updated, to be used for integral and derivative computations
 *
 *
 */
typedef struct pid_controller_t {
	// Setpoint
	int32_t setpoint;
	// PID Coefficients
	int16_t kp; // Proportional
	int16_t ki; // Integral
	int16_t kd; // Derivitive
	// Error
	int32_t prev_error;  // Used in Derivitive Calc
	int32_t prev_i;
	int32_t max_i;
	// Integral/Derivitive dt calculation
	uint32_t prev_time;
} pid_controller_t;


/**
 *
 * Resets the error and previous timestep of the PID controller.
 *
 * Sets the gain parameters, resets the error's and time
 *
 * @param pid_handle - a handle to the PID structure to initialize
 * @param curr_time - the current time for prev_time to be reset to
 */
void PID_Reset(pid_controller_t *pid_handle);

/**
 * Updates the PID controller and returns the computed output
 *
 * @param pid_handle - a handle to the PID structure to update
 * @param stpt - the setpoint of the system
 * @param meas - the current measurement of the value
 * @return
 */
int32_t PID_Step(pid_controller_t *pid_handle, int32_t meas);

#endif
/** @}*/
