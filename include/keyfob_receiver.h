#ifndef _KEYFOB_RECEIVER_H_
#define _KEYFOB_RECEIVER_H_

#include "terminal.h"

/** @brief Keyfob Receiver Module
 *
 * @defgroup keyfob Keyfob Receiver Module
 *
 * Dependencies:
 * eeprom module (direct)
 * hal_eeprom module
 * task module (direct)
 * list module
 * timing module
 * hal_keyfob_receiver module (direct)
 * 
 * This module handles receiving pulse data from a keyfob remote, learning and
 * saving remote serial numbers, and calling a user callback with the data 
 * received.
 * 
 * Users of the module should call Keyfob_Init() with the name of the callback
 * function as an input. The hardware abstraction layer only needs to provide a
 * microsecond pulse time between every edge of the signal.
 * 
 * The module was originally designed for the remotes used by Touch-N-Go and
 * LEDGlow and should work for many other unencrypted remotes. The standard 
 * format is to have preamble consisting of a short 1 and a very long 0, the
 * very long 0 is detected as the start of the message. After the preamble all
 * data is formatted approximately as 3 time units high 1 time unit low for a 1
 * or 1 time unit high and 3 time units low for a 0.
 * 
 * By default the module expects a 32 time unit preamble followed by 20 serial
 * number bits followed by 4 data bits with a time unit ranging from 280 to 520
 * microseconds. These defaults can be override as needed using #define in
 * system.h
 *
 * This module depends on the EEPROM module in order to save the learned/paired
 * remotes.
 * 
 * Example usage:
 * @code
 * // in system.h
 * #define KEYFOB_SERIAL_BITS num // optional, default is 20
 * #define KEYFOB_INTERMEDIATE_BITS num // optional, default is 0
 * #define KEYFOB_DATA_BITS num // optional, default is 4
 * #define KEYFOB_POST_DATA_BITS num // optional, default is 0
 * #define KEYFOB_NUM_REMOTES num // optional, default is 2
 * #define KEYFOB_EE_BASE address // optioanl, default is 100
 * // any other defines required by hal_keyfob_receiver for your target MCU
 * 
 * // wherever you need to use the keyfob receiver (e.g. main.c?)
 * void MyDataReceiver(uint8_t data) {
 *     switch(data) {
 *         // do something with the received data
 *     }
 * }
 * 
 * Keyfob_Init(MyDataReceiver);
 * // ...
 * Keyfob_EnableLearning(); // enable remotes to be learned
 * // ...
 * Keyfob_DisableLearning(); // disable remotes from being learned
 * @endcode
 * 
 * Tested on PIC24FJ128GA202 with LEDGlow Keyfobs
 * 
 * @author Michael Muhlbaier
 *
 * @{
 */

/** Note: hal_keyfob_receiver.h may override the defaults to avoid MCU intense
 * division
 */
#include "hal_keyfob_receiver.h"

#ifndef KEYFOB_SERIAL_BITS
#define KEYFOB_SERIAL_BITS 20
#endif

// some keyfobs may include an intermediate bit like a low voltage detect bit
#ifndef KEYFOB_INTERMEDIATE_BITS
#define KEYFOB_INTERMEDIATE_BITS 0
#endif

#ifndef KEYFOB_DATA_BITS
#define KEYFOB_DATA_BITS 4
#endif

// some keyfobs may include bits after the data like a checksum
#ifndef KEYFOB_POST_DATA_BITS
#define KEYFOB_POST_DATA_BITS 0
#endif

#ifndef KEYFOB_MIN_TIME_UNIT
#define KEYFOB_MIN_TIME_UNIT 280
#endif

#ifndef KEYFOB_MAX_TIME_UNIT
#define KEYFOB_MAX_TIME_UNIT 520
#endif

#define KEYFOB_PREAMBLE_UNITS 32

#ifndef KEYFOB_NUM_REMOTES
#define KEYFOB_NUM_REMOTES 2
#endif

#ifndef KEYFOB_EE_BASE
#define KEYFOB_EE_BASE 100
#endif

#ifndef KEYFOB_CONSECUTIVE_DATA
#define KEYFOB_CONSECUTIVE_DATA 2
#endif

#ifndef KEYFOB_BIT_TIMEOUT
#define KEYFOB_BIT_TIMEOUT KEYFOB_MAX_TIME_UNIT * 3
#endif

#ifndef KEYFOB_CONSECUTIVE_SERIALS
#define KEYFOB_CONSECUTIVE_SERIALS 4
#endif

/** @brief Initialize keyfob receiver module and associated HAL module
 * 
 * Initialized keyfob module, hal_keyfob_receiver, and registers a callback
 * function to be called when data is received.
 * 
 * Learning mode defaults to off.
 * 
 * @param callback function pointer to user function which will be called with
 * data received when receiving data from a remote with a saved serial number
 */
void Keyfob_Init(void (*callback)(uint8_t));

/** @brief Enable learning of new keyfob remotes
 * 
 * This function must be called to learn any new remotes. All existing remotes
 * will remain until KEYFOM_NUM_REMOTES has been saved and then new remotes will
 * overwrite old remotes.
 */
void Keyfob_EnableLearning(void);

/** @brief Disable learning of new remotes (default)
 * 
 * This function only needs to be called after calling Keyfob_EnableLearning()
 * to prevent the learning of additional keyfob remotes.
 */
void Keyfob_DisableLearning(void);

/** @brief used by hal_keyfob_receiver to pass a pulse time in microseconds to
 * the keyfob receiver module which takes care of decoding etc.
 * 
 * @param pulse time between edges (rising or falling) in microseconds
 */
void Keyfob_ProcessPulse(uint16_t pulse);

///@}
#endif // _KEYFOB_RECEIVER_H_
